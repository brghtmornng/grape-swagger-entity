class Core::Service::Exception
  class Data < StandardError;  end
  class Authorization < StandardError; end
  class Permissions < StandardError; end
  class Record < StandardError
    attr_accessor :record
    def initialize(record)
      @record = record
    end
  end
end