class Core::Service::Base
  require_relative 'concern/authorized'
  require_relative 'concern/error'
  include Core::Service::Authorized
  include Core::Service::Error
end