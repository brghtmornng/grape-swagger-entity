module Core
  class Engine < ::Rails::Engine
    isolate_namespace Core

    config.autoload_paths += %W(#{Core::Engine.root}/lib/)

    rake_tasks do
      load 'lib/tasks/api.rake'
    end

    initializer '01.core.api' do |app|
      GrapeSwaggerRails.options.app_url  = Rails.configuration.action_controller.asset_host
      GrapeSwaggerRails.options.url      = "api/v2/specification.json"
      GrapeSwaggerRails.options.app_name = Rails.application.class.name
    end

    initializer '02.core.kaminari' do |app|
      require 'kaminari'
      Kaminari.configure do |config|
        # config.default_per_page = 25
        # config.max_per_page = nil
        # config.window = 4
        # config.outer_window = 0
        # config.left = 0
        # config.right = 0
        # config.page_method_name = :page
        # config.param_name = :page
        # config.params_on_first_page = false
      end
    end

    config.before_configuration do |app|
      app.config.paths.add File.join('app', 'api'), glob: File.join('**', '*.rb')
      app.config.autoload_paths += Dir[Rails.root.join('app')]
    end

    routes.draw do
      require 'grape-swagger-rails'
      require 'grape-swagger'

      mount GrapeSwaggerRails::Engine => '/swagger'
    end
  end
end