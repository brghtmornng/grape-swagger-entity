class Core::API::Entities::Pagination < Core::API::Entity
  expose :total, documentation: { type: 'Integer', desc: 'Total number of items available', required: true }
  expose :page, documentation: { type: 'Integer', desc: 'Position in pagination', required: true }
  expose :per_page, documentation: { type: 'Integer', desc: 'Number of items to retrieve (100 max)', required: true }
  expose :offset, documentation: { type: 'Integer', desc: 'Page offset to fetch', required: true }

  private
  def offset
    options[:offset]
  end

  def per_page
    options[:per_page]
  end

  def page
    options[:page]
  end

  def total
    options[:total]
  end
end