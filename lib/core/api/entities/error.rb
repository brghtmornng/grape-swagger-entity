class Core::API::Entities::Error < Core::API::Entity
  class << self
    attr_reader :determine_errors_handle
    attr_reader :determine_backtrace_handle
    def backtrace &block
      @determine_backtrace_handle = block
    end
    def errors &block
      @determine_errors_handle = block
    end
  end

  expose :success, documentation: { type: 'Boolean', required: true, desc: 'Falsey value' }

  expose :errors,
         documentation: { type: 'String', is_array: true, desc: 'Collection of error messages', required: true }

  expose :backtrace,
         documentation: { type: 'String', is_array: true, desc: 'Error backtrace', required: false },
         if: lambda { |object, options| self.backtrace.present? }


  def backtrace
    options[:backtrace]
    # @backtrace ||= _backtrace
  end

  def success
    false
  end

  def errors
    @errors ||= _errors
  end

  private
  def _errors
    errors = object
    if self.class.determine_errors_handle.respond_to? :call
      errors = self.class.determine_errors_handle.call(object, options)
    end
    errors = [errors] unless errors.is_a? Array
    errors
  end

  def _backtrace
    if self.class.determine_backtrace_handle.present?
      backtrace ||= self.class.determine_backtrace_handle.call(object, options)
    end
    backtrace
  end
end