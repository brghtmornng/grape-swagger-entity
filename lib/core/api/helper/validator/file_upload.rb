class Core::API::Helper::Validator::FileUpload < Grape::Validations::Base
  def validate_param!(attr_name, params)
    element = params[attr_name]
    if element.present? && element != 'null' && (element.has_key? :data64)
      picture = Paperclip.io_adapters.for("data:#{element[:content_type]};base64,#{element[:data64]}")
      picture.original_filename = element[:file_name] || "picture.#{MIME::Types[picture.content_type].first.extensions.first}"
      params[attr_name] = picture
    end
    if element.present? && element != 'null'
      params[attr_name] = ActionDispatch::Http::UploadedFile.new(element) unless element.has_key? :data64
    end
    params[attr_name]
  end
end