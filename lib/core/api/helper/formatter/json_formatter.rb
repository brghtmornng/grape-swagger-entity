module Core::API::Helper::Formatter::JsonFormatter
  class << self
    def call(results, env)
      if is_file? env
        results
      else
        Core::API::Entities.fetch_entity(endpoint(env)).new(results || {}, options(env)).to_json
      end
    end

    def is_file?(env)
      endpoint(env).header.has_key?('Content-Disposition')
    end

    def options(env)
      endpoint = endpoint(env)
      options = endpoint.route_setting(:entity) || {}
      if endpoint.header['X-Total'].present?
        options.merge!({
                           total: endpoint.header['X-Total'].to_i,
                           page: endpoint.header['X-Page'].to_i,
                           per_page: endpoint.header['X-Per-Page'].to_i,
                           offset: endpoint.header['X-Offset'].to_i,
                       })
      end
      options
    end

    def endpoint(env)
      env['api.endpoint']
    end
  end
end