module Core::API::Helper::Formatter::ErrorFormatter
  def self.call(message, backtrace, options, env)
    Core::API::Entities::Error.represent(message, { backtrace: backtrace }).to_json
  end
end