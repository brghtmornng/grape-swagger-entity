module Core::API::Helper::Auth
  extend ActiveSupport::Concern
  AUTHORIZED = 'Access token is missing, expired or invalid'
  FORBIDDEN = 'Access forbidden, invalid user role'

  included do
    before do
      auth_service if route_setting(:authorized)
    end

    before do
      auth_service.allow! *route_setting(:allow) if route_setting :allow
    end

    before do
      auth_service.deny! *route_setting(:deny) if route_setting :deny
    end

    helpers do
      def from_bearer_authorization(header)
        pattern = /^Bearer /i
        token_from_header(header, pattern) if header.present? && header.match(pattern)
      end

      def token_from_header(header, pattern)
        header.gsub pattern, ''
      end

      def token_from_request
        from_bearer_authorization(headers['Authorization']) || ''
      end

      def auth_service
        error!('Need to implement auth service method', 500)
      end

      # TODO Need to refactor
      def current_user
        auth_service.current_user
      rescue Exception => e
        nil
      end

      def current_user!
        auth_service.current_user
      end

      def current
        current_user
      end
    end
  end

  class_methods do
    def allow(*roles)
      route_setting :allow, roles
      route_start &:http_forbidden
      # route_start &:auth_header
    end

    def deny(*roles)
      route_setting :deny, roles
      route_start &:http_forbidden
      # route_start &:auth_header
    end

    def authorized!
      route_setting :authorized, true
      route_start &:http_authorized
      # route_start &:auth_header
    end

    def organization!
      route_setting :organization, true
      route_start &:http_authorized
      route_start &:organization_header
    end

    def authorized_with_organization!
      route_setting :organization, true
      route_start &:http_authorized
      route_start &:organization_header
    end

    def http_authorized(message = AUTHORIZED)
      http_code 401, message, Core::API::Entities::Error
    end

    def auth_header
      headers({ 'Authorization' => { description: 'Http header with access_token', required: true, default: 'Bearer' } })
    end

    def organization_header
      headers({ 'Authorization' => { description: 'Http header with access_token', required: true, default: 'Bearer' },
                'Organization-Id' => { description: 'Organization Id ', required: false, default: '' } })
    end

    def http_forbidden(message = FORBIDDEN)
      http_code 403, message, Core::API::Entities::Error
    end
  end
end