module Core::API::Helper::DateRange
  extend ActiveSupport::Concern
  included do
    class << self
      def route(*args)
        (global_setting(:route_start) || []).each { |block| block.call self }
        (route_setting(:route_start) || []).each { |block| block.call self }
        super *args
      end

      def route_start(&block)
        route_start = route_setting(:route_start) || []
        route_start << block
        route_setting(:route_start, route_start)
      end

      def global_route_start(&block)
        route_start = global_setting(:route_start) || []
        route_start << block
        global_setting(:route_start, route_start)
      end
    end
  end
end