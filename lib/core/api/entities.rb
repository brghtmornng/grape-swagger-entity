require 'grape-entity'
module Core::API::Entities
  extend ActiveSupport::Autoload
  SINGLE = 'Single'
  COLLECTION = 'Collection'
  PAGINATION = 'Pagination'
  PAGINATION_DATA = 'Data'
  eager_autoload do
    autoload :Success
    autoload :Error
    autoload :Pagination
  end
  class << self
    # Generate single wrapper
    def generate_single(key, klass)
      class_name = klass.to_s.gsub('::', '')
      class_name = "#{class_name}#{SINGLE}"
      return const_get class_name if const_defined?(class_name)
      single = Class.new(Core::API::Entities::Success) do
        expose key, using: klass, documentation: { type: klass, param_type: 'body', required: true }
        alias_method key, :object
      end
      const_set class_name, single
      single
    end

    # Generate collection wrapper
    def generate_collection(key, klass)
      class_name = klass.to_s.gsub('::', '')
      class_name = "#{class_name}#{COLLECTION}"
      return const_get class_name if const_defined?(class_name)
      collection = Class.new(Core::API::Entities::Success) do
        expose key, using: klass, documentation: { type: klass, is_array: true, required: true }
        alias_method key, :object
      end
      const_set class_name, collection
      collection
    end

    # Generate paginate wrapper
    def generate_paginate(key, klass)
      class_name = klass.to_s.gsub('::', '')
      class_name = "#{class_name}#{PAGINATION}"
      return const_get class_name if const_defined?(class_name)
      pagination_meta = Class.new(Core::API::Entities::Pagination) do
        present_collection true
        expose :items, using: klass, documentation: { type: klass, is_array: true, required: true }
      end

      pagination_meta_class_name = "#{class_name}#{PAGINATION_DATA}"

      const_set pagination_meta_class_name, pagination_meta


      paginate = Class.new(Core::API::Entities::Success) do
        alias_method key, :object
        expose key, using: pagination_meta, documentation: { type: pagination_meta, required: true }
      end

      const_set class_name, paginate
      paginate
    end

    def fetch_entity(endpoint)
      entity = endpoint.route_setting(:raw_entity) || {}
      klass = entity.fetch(:klass, nil)
      key = entity.fetch(:key, nil)
      if key.present?
        if with_pagination? endpoint
          Core::API::Entities.generate_paginate(key, klass)
        elsif entity.fetch(:is_array, false)
          Core::API::Entities.generate_collection(key, klass)
        else
          Core::API::Entities.generate_single(key, klass)
        end
      else
        klass
      end
    end

    def with_pagination?(endpoint)
      description = endpoint.route_setting(:description)
      route_params = description ? description.fetch(:params, {}) : {}
      entity = endpoint.route_setting(:entity) || {}
      pagination_params = ['page', 'per_page', 'offset']
      entity.fetch(:pagination, false) || pagination_params.all?(&route_params.method(:has_key?))
    end

    def grape_entity?(klass)
      klass.try(:ancestors).try(:include?, Grape::Entity)
    end
  end
end