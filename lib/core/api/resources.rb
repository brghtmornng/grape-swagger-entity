class Core::API::Resources < Grape::API
  require_relative 'helper/validator'
  require_relative 'helper/callbacks'
  require_relative 'helper/exceptions'
  require_relative 'helper/date_range'
  require_relative 'helper/formatter'
  require_relative 'helper/auth'
  require_relative 'helper/desc'
  require_relative 'helper/logger'

  def self.inherited(subclass)
    super subclass
    subclass.include Grape::Kaminari
    subclass.include Core::API::Helper::Callbacks
    subclass.include Core::API::Helper::Exceptions
    subclass.include Core::API::Helper::Formatter
    subclass.include Core::API::Helper::DateRange
    # include API::Helper::Logger
    subclass.include Core::API::Helper::Desc
    subclass.include Core::API::Helper::Auth
  end
end
