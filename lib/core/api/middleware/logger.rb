# class Grape::Middleware::Error
#   def error_response(error={})
#     # super
#   end
# end
class Core::API::Middleware::Logger < Grape::Middleware::Base
  HEADERS_BLACKLIST = ['HTTP_COOKIE'].freeze
  FILTERS = Rails.application.config.filter_parameters << :tempfile

  # def call!(env)
  #   @env = env
  #   before
  #   error = catch(:error) do
  #     begin
  #       @app_response = @app.call(@env)
  #     rescue => e
  #       after_exception(e)
  #       raise e
  #     end
  #     nil
  #   end
  #   if error
  #     after_failure(error)
  #     throw(:error, error)
  #   else
  #     status, _, _ = *@app_response
  #     after
  #   end
  #   @app_response
  # end

  def after_exception(e)
    Rails.logger.debug "[api] Exception: #{e.message}"
    Rails.logger.debug "[api] Backtrace: \n #{e.backtrace.join "\n"}"
  end

  def after_failure(error)
    Rails.logger.info %Q(  Error: #{error[:message]}) if error[:message]
    # after(error[:status])
  end

  def log_error_response(start, &block)
    result = block.call
    after(result, start)
  end

    # def after(result, start)
    #   stop = Time.now
    #
    #   milliseconds_taken ||= ((stop - start) * 1000).to_i
    #
    #   response_status = result[0]
    #   response_headers = result[1]
    #   response_headers = response_headers.select {|k,v| k.start_with?('HTTP_') && !::API::Middleware::Logger::HEADERS_BLACKLIST.include?(k)}
    #                          .collect {|pair| [pair[0].sub(/^HTTP_/, ''), pair[1]]}
    #                          .collect {|pair| pair.join(": ")}
    #                          .sort
    #
    #   parts = []
    #   result[2].each{|part| parts << part}
    #   response_body = parts.join
    #
    #   Rails.logger.debug "[api] Response headers: #{response_headers}"
    #   Rails.logger.debug "[api] Response body: #{response_body}"
    #   Rails.logger.info "[api] Completed #{response_status} in #{milliseconds_taken}ms"
    #
    #   result
    # end
  # end

  def before
    @start = Time.now
    data = parameters

    Rails.logger.info
    Rails.logger.info "[api] Started #{request_method} \"#{request_target}\" for #{request_ip} at #{Time.now}"
    Rails.logger.debug "[api] Request headers: #{request_headers}"
    Rails.logger.debug "[api] Request query: #{data[:GET].to_json}"
    Rails.logger.debug "[api] Request body: #{data[:POST].to_json}"
    Rails.logger.debug "[api] Source: #{source_file}:#{source_line}"
  end

  def after
    @stop = Time.now

    Rails.logger.debug "[api] Response headers: #{response_headers}"
    Rails.logger.debug "[api] Response body: #{response_body}"
    Rails.logger.info "[api] Completed #{response_status} in #{milliseconds_taken}ms"

    @app_response
  end


  private

  def response_status
    @app_response.status
  end

  def response_headers
    @app_response.header
  end

  def response_body
    body = @app_response.body
    case @app_response.body
      when Array
        parts = []
        @app_response.body.each{|part| parts << part}
        parts.join
      when File
        "[File #{body.size}]"
      else
        '[Unknown body type]'
    end
  end


  def milliseconds_taken
    @milliseconds_taken ||= ((@stop - @start) * 1000).to_i
  end


  def request_log_data
    @request_log_data ||= create_request_log_data
  end

  def request_method
    env['REQUEST_METHOD']
  end

  def request_target
    env['PATH_INFO']
  end

  def request_ip
    env['REMOTE_ADDR']
  end

  def request_headers
    headers = env.select {|k,v| k.start_with?('HTTP_') && !HEADERS_BLACKLIST.include?(k)}
                  .collect {|pair| [pair[0].sub(/^HTTP_/, ''), pair[1]]}
                  .collect {|pair| pair.join(": ")}
                  .sort
  end

  def parameters
    f = ActionDispatch::Http::ParameterFilter.new(FILTERS)
    request = ::Rack::Request.new(env)
    results = {}
    query = f.filter request.GET
    results[:GET] = query
    if request.post?
      if request.content_type.try(:include?, 'application/json')
        data = request.body.read
        request.body.rewind
        body = JSON.parse data if data.present? rescue nil
      else
        body = f.filter request.POST
      end
      if body.present?
        body = f.filter body
        results[:POST] = body
      end
    end
    results
  end

  def request_params
    env['rack.input'].read
  end


  def source_file
    env['api.endpoint'].source.source_location[0][(Rails.root.to_s.length+1)..-1]
  end

  def source_line
    env['api.endpoint'].source.source_location[1]
  end
end