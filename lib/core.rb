require 'active_support'
module Core
  extend ActiveSupport::Autoload
  autoload :API, 'core/api/api'
  autoload :Service, 'core/service/service'
  require 'core/engine' if defined?(::Rails)
end