require 'spec_helper'
# require 'entities/success'

describe Core::API::Entities::Success do

  let(:instance) { described_class.new Object.new }

  it 'expect to have boolean success field' do
    expect(instance.as_json[:success]).to be_truthy
  end
end