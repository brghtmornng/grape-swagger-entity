$LOAD_PATH.push File.expand_path('../lib', __FILE__)
require 'core/version'

Gem::Specification.new do |s|
  s.name        = 'core'
  s.version     = Core::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ['VR']
  s.email       = ['aleksee44@gmail.com']
  s.summary     = 'A simple Ruby framework for building REST-like APIs.'
  s.required_ruby_version = '>= 2.3.1'

  s.add_runtime_dependency 'activesupport'
  s.add_runtime_dependency 'grape', '0.19.0'
  s.add_runtime_dependency 'grape-swagger', '0.25.3'
  s.add_runtime_dependency 'grape-swagger-entity', '0.1.5'
  s.add_runtime_dependency 'grape-swagger-representable', '0.1.3'
  s.add_runtime_dependency 'grape-entity', '0.6.0'
  s.add_runtime_dependency 'kaminari', '1.0.0'
  s.add_runtime_dependency 'kaminari-grape', '1.0.0'
  s.add_runtime_dependency 'grape-kaminari', '0.1.9'
  s.add_runtime_dependency 'grape-swagger-rails', '0.3.0'


  s.files         = Dir['**/*'].keep_if { |file| File.file?(file) }
  s.test_files    = Dir['spec/**/*']
  s.require_paths = ['lib', 'lib/core']
end